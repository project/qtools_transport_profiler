<?php

namespace Drupal\qtools_transport_profiler;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Connection;
use Drupal\Core\State\State;
use Drupal\qtools_common\QtoolsCacheHelper;
use Drupal\qtools_profiler\PerformanceService as ProfilerPerformanceService;

/**
 * Service to integrate with qtools_profiler.
 */
class PerformanceService {

  /**
   * Drupal\Core\Database\Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Drupal\Core\State\State.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Configuration.
   *
   * @var array
   */
  protected $configuration = [
    'logging_mode' => 0,
    'trace_length' => 0,
  ];

  // Table names.
  const TABLE_SERVICE = 'qtools_transport_profiler_service';
  const TABLE_CALLS = 'qtools_transport_profiler_calls';
  const TABLE_ERRORS = 'qtools_transport_profiler_errors';

  const LOG_DETAILS_NONE = 0;
  const LOG_DETAILS_ERRORS = 1;
  const LOG_DETAILS_ALL = 2;
  const LOG_DETAILS_ALL_WITH_STATIC = 3;

  // Config key.
  const STATE_CONFIGURATION_KEY = 'qtools_transport_profiler_configuration';

  /**
   * Constructor.
   */
  public function __construct(
    Connection $connection,
    State $state
  ) {
    $this->connection = $connection;
    $this->state = $state;

    // Read configuration from state and merge with defaults.
    $activeConfiguration = $this->state->get(static::STATE_CONFIGURATION_KEY, []);
    foreach ($this->configuration as $key => $value) {
      if (isset($activeConfiguration[$key])) {
        $this->configuration[$key] = $activeConfiguration[$key];
      }
    }
  }

  /**
   * Determine if performance service is currently active.
   */
  public function enabled() {
    // @todo Implement real check.
    return TRUE;
  }

  /**
   * Returns current login mode.
   */
  public function getLoggingMode() {
    return $this->confGet('logging_mode');
  }

  /**
   * Saves request details to database.
   */
  protected function saveDetails($rid, $details) {

    // Get details records.
    $records = $this->getDetailsRecords($rid, $details);

    // Save parsed values to database.
    if (!empty($records)) {
      $query = $this->connection
        ->insert(static::TABLE_ERRORS)
        ->fields(array_keys($records[0]));

      foreach ($records as $row) {
        $query->values($row);
      }
      $query->execute();
    }
  }

  /**
   * Get details records.
   */
  protected function getDetailsRecords($rid, $details) {
    $records = [];

    foreach ($details as $sid => $list) {
      foreach ($list as $call_info) {
        [$code, $message] = $this->getDetailsRecordsCodeMessage($call_info);

        // Prepare data package .
        $records[$call_info['order']] = [
          'rid' => $rid,
          'sid' => $sid,
          'cache_op' => $call_info['mode'],
          'time' => $call_info['time'],
          'code' => (int) $code,
          'message' => substr($message, 0, 255),
          'data' => !empty($call_info['details']) ? Json::encode($call_info['details']) : NULL,
        ];
      }
    }

    // Order details in the order of appearance.
    ksort($records);
    return array_values($records);
  }

  /**
   * Gets code and message for given record.
   */
  protected function getDetailsRecordsCodeMessage($call_info) {
    if (!empty($call_info['error'])) {
      $code = $call_info['error']['code'];
      $message = $call_info['error']['message'];
    }
    else {
      $code = 0;
      if (!empty($call_info['details']['request']['service'])) {
        $message = is_array($call_info['details']['request']['service']) ?
          implode('|', $call_info['details']['request']['service']) :
          $call_info['details']['request']['service'];
        if (!empty($call_info['details']['request']['response'])) {
          $message .= ' *';
        }
        if (strpos($call_info['details']['request']['config']['path'], 'api/v2')) {
          $include_shipping = $call_info['details']['options']['query']['includeShipping'] ?
            ' S' :
            '';
          $message .= ' (v2)' . $include_shipping;
        }
      }
      else {
        $message = '';
      }
    }
    return [$code, $message];
  }

  /**
   * Parses raw stats into records and details to be stored in db.
   */
  protected function parseStats($rid, $stats) {
    // Convert stats into plain array.
    $records = [];
    $details = [];

    // Static calls are grouped and not present in this list.
    $keys = [
      QtoolsCacheHelper::CACHE_FOUND_NONE => 'call',
      QtoolsCacheHelper::CACHE_FOUND_PERSISTENT => 'cache',
      QtoolsCacheHelper::CACHE_FOUND_STATIC => 'static',
    ];

    foreach ($stats as $service => $calls) {
      $sid = $this->getServiceId($service);
      $info = [
        'rid' => $rid,
        'sid' => $sid,
        'call_count' => 0,
        'call_time' => 0,
        'cache_count' => 0,
        'cache_time' => 0,
        'static_count' => 0,
        'static_time' => 0,
        'offline_count' => 0,
        'error_count' => 0,
      ];

      foreach ($calls as $call_info) {
        $key = $keys[$call_info['mode']];

        // Update time metric.
        $info[$key . '_count'] += 1;
        $info[$key . '_time'] += $call_info['time'];

        $info['static_count'] += $call_info['static_count'];
        $info['static_time'] += $call_info['static_time'];

        // Update errors metrics only for real requests.
        if ($call_info['mode'] == QtoolsCacheHelper::CACHE_FOUND_NONE && !empty($call_info['error'])) {
          $info['error_count']++;

          // Log details for errors (with or without actual details).
          $details[$sid][] = $call_info;
        }
        elseif (!empty($call_info['details'])) {
          // Log details if any actual details.
          $details[$sid][] = $call_info;
        }
      }
      $records[] = $info;
    }

    return [
      'records' => $records,
      'details' => $details,
    ];
  }

  /**
   * Saves performance stats to database.
   */
  public function savePerformanceStats($rid, $stats) {
    // Convert stats into plain array.
    $parsed = $this->parseStats($rid, $stats);
    $records = $parsed['records'];
    $details = $parsed['details'];

    // Save parsed values to database.
    if (!empty($records)) {
      $query = $this->connection
        ->insert(static::TABLE_CALLS)
        ->fields(array_keys($records[0]));

      foreach ($records as $row) {
        $query->values($row);
      }
      $query->execute();
    }

    // Save details.
    if (!empty($details)) {
      $this->saveDetails($rid, $details);
    }

  }

  /**
   * Gets service id or add new one in the database.
   *
   * @return int
   *   Id of the service.
   */
  protected function getServiceId($service) {
    $lookup_table = &drupal_static('qtools_transport_profiler_performance_service_service_ids', []);
    if (!empty($lookup_table[$service])) {
      return $lookup_table[$service];
    }

    // Lookup for route in database.
    $result = $this->connection->select(static::TABLE_SERVICE, 's')
      ->fields('s', ['id'])
      ->condition('service', $service)
      ->range(0, 1)
      ->execute()
      ->fetchObject();

    if (!empty($result)) {
      $id = $result->id;
    }
    else {
      $id = $this->connection->insert(static::TABLE_SERVICE)
        ->fields(['service' => $service])
        ->execute();
    }

    // Update static cache.
    $lookup_table[$service] = $id;

    return $id;
  }

  /**
   * Returns current configuration.
   *
   * @return array|mixed
   *   Configuration.
   */
  public function confGet($key = NULL) {
    return empty($key) ? $this->configuration : $this->configuration[$key];
  }

  /**
   * Returns current configuration.
   */
  public function confSet($newConfiguration) {
    foreach ($this->configuration as $key => $value) {
      if (isset($newConfiguration[$key])) {
        $this->configuration[$key] = $newConfiguration[$key];
      }
    }

    // Cleanup logs on each logs disable.
    if ($this->configuration['logging_mode'] == static::LOG_DETAILS_NONE) {
      $this->logsCleanUp();
    }
    $this->state->set(static::STATE_CONFIGURATION_KEY, $this->configuration);
  }

  /**
   * Truncates error logs.
   */
  protected function logsCleanUp() {
    $this->connection->truncate(static::TABLE_ERRORS);
  }

  /**
   * Flush all logs.
   */
  public function flush() {
    $this->connection->truncate(static::TABLE_ERRORS)->execute();
    $this->connection->truncate(static::TABLE_SERVICE)->execute();
    $this->connection->truncate(static::TABLE_CALLS)->execute();
  }

  /**
   * Flush all logs up until given request id.
   */
  public function cleanup($rid) {
    // Clean up own tables.
    $this->connection->delete(static::TABLE_ERRORS)
      ->condition('rid', $rid, '<')
      ->execute();
    $this->connection->delete(static::TABLE_CALLS)
      ->condition('rid', $rid, '<')
      ->execute();
  }

  /**
   * Get Request details.
   *
   * @return array|null
   *   Summary definitions array.
   */
  public function getRequestSummary($requestId, $stats = NULL) {
    // Check if table still exists.
    if (!$this->connection->schema()->tableExists(static::TABLE_CALLS)) {
      return NULL;
    }

    // If we are working with current request we need to check buffer
    // instead of log table.
    if ($stats !== NULL) {
      $parsed = $this->parseStats($requestId, $stats);
      $calls = $parsed['records'];
    }
    else {
      // Response details.
      $query = $this->connection->select(static::TABLE_CALLS, 'c');
      $query->fields('c');
      $query->condition('rid', $requestId);
      $calls = $query->execute()->fetchAll();
    }

    $info = [
      'time' => 0,
      'count' => 0,
      'cache' => 0,
      'static' => 0,
      'errors' => 0,
    ];

    $stats = [
      'time' => 'call_time',
      'count' => 'call_count',
      'cache' => 'cache_count',
      'static' => 'static_count',
      'errors' => 'error_count',
    ];

    foreach ($calls as $call) {
      $call = (array) $call;
      foreach ($stats as $key => $field) {
        $info[$key] += $call[$field];
      }
    }

    $summary = [
      ['s-time', round($info['time'], 3), 'sec'],
      ['s-count', $info['count'], ''],
      ['s-cache', $info['cache'] + $info['static'], ''],
      [
        's-errors',
        $info['errors'],
        '',
        !empty($info['errors']) ? ProfilerPerformanceService::SUMMARY_STAT_TYPE_ERROR : NULL,
      ],
    ];

    return $summary;
  }

}
