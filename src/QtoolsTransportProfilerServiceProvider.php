<?php

namespace Drupal\qtools_transport_profiler;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\qtools_transport\QtoolsTransportService;

/**
 * Service provider for the Qtools Profiler for Qtools Transport/API module.
 */
class QtoolsTransportProfilerServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides transport service with one that logs requests.
    $definition = $container->getDefinition(QtoolsTransportService::class);
    $definition->setClass('Drupal\qtools_transport_profiler\TransportServiceWrapper');
  }

}
