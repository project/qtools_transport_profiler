<?php

namespace Drupal\qtools_transport_profiler\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\qtools_transport_profiler\PerformanceService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure transport profiler.
 */
class ProfilerConfigurationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected PerformanceService $performanceService,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(PerformanceService::class)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profiler_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $conf = $this->performanceService->confGet();

    $form['conf'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration'),
      '#tree' => TRUE,
    ];

    $options = [
      PerformanceService::LOG_DETAILS_NONE => $this->t('None'),
      PerformanceService::LOG_DETAILS_ERRORS => $this->t('Errors'),
      PerformanceService::LOG_DETAILS_ALL => $this->t('All'),
      PerformanceService::LOG_DETAILS_ALL_WITH_STATIC => $this->t('All with static calls'),
    ];
    $form['conf']['logging_mode'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('Log request details'),
      '#default_value' => !empty($conf['logging_mode']) ? $conf['logging_mode'] : array_keys($options)[0],
      '#description' => $this->t('Warning! Enabling this option will log all request info into database, that may expose user personal settings. When set to NONE all logs will be erased.'),
    ];

    $form['conf']['trace_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Backtrace length'),
      '#default_value' => !empty($conf['trace_length']) ? $conf['trace_length'] : 0,
      '#description' => $this->t('This will log simple backtrace into request details.'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $conf = $form_state->getValue('conf');
    $this->performanceService->confSet($conf);
  }

}
