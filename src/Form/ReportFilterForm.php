<?php

namespace Drupal\qtools_transport_profiler\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\qtools_transport_profiler\ReportService;

/**
 * Filters forms for reports.
 */
class ReportFilterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected ReportService $reportService,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(ReportService::class)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'qtools_transport_profiler_report_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $dateFormat = 'Y-m-d';
    $timeFormat = 'H:i';
    $filters = $this->reportService->getReportFilters();

    $form['filters'] = [
      '#tree' => TRUE,
    ];
    $form['filters']['start'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start DateTime'),
      '#date_date_format' => $dateFormat,
      '#date_time_format' => $timeFormat,
      '#default_value' => DrupalDateTime::createFromTimestamp($filters['start']),
    ];

    $form['filters']['end'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End DateTime'),
      '#date_date_format' => $dateFormat,
      '#date_time_format' => $timeFormat,
      '#default_value' => DrupalDateTime::createFromTimestamp($filters['end']),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['apply'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#button_type' => 'primary',
      '#button_value' => 'apply',
    ];
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#button_value' => 'reset',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#button_value'] == 'reset') {
      $filters = [];
    }
    else {
      $filters = $form_state->getValue('filters');
      $filters['start'] = strtotime($filters['start']);
      $filters['end'] = strtotime($filters['end']);
    }
    $this->reportService->setReportFilters($filters);
  }

}
