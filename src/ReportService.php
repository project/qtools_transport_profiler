<?php

namespace Drupal\qtools_transport_profiler;

use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\qtools_common\QtoolsCacheHelper;
use Drupal\qtools_profiler\PerformanceService;
use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\filter\Render\FilteredMarkup;

/**
 * Service to produce profiler reports.
 */
class ReportService {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Database\Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Drupal\qtools_profiler\PerformanceService.
   *
   * @var \Drupal\qtools_profiler\PerformanceService
   */
  protected $performanceService;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  // Table names.
  const TABLE_SERVICE = 'qtools_transport_profiler_service';
  const TABLE_CALLS = 'qtools_transport_profiler_calls';
  const TABLE_ERRORS = 'qtools_transport_profiler_errors';
  const ITEMS_PER_PAGE = 15;

  /**
   * Constructor.
   */
  public function __construct(Connection $connection, PerformanceService $performanceService, Renderer $renderer) {
    $this->connection = $connection;
    $this->performanceService = $performanceService;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function export(MessengerInterface $messenger, $input, $name = NULL) {
    if (class_exists('\Symfony\Component\VarDumper\Cloner\VarCloner')) {
      $cloner = new VarCloner();
      $dumper = 'cli' === PHP_SAPI ? new CliDumper() : new HtmlDumper();

      $output = fopen('php://memory', 'r+b');
      $dumper->dump($cloner->cloneVar($input), $output);
      $output = stream_get_contents($output, -1, 0);
    }
    else {
      $output = $this->t('Symfony VarDumper is not available.')->render();
    }

    if ($name) {
      $output = $name . ' => ' . $output;
    }

    $export = FilteredMarkup::create($output);
    $messenger->addMessage($export, MessengerInterface::TYPE_STATUS, TRUE);
  }

  /**
   * Sets report filter values.
   */
  public function setReportFilters($filters) {
    $_SESSION['qtools_transport_profiler']['filters'] = $filters;
  }

  /**
   * Load error details by id.
   *
   * @return mixed
   *   Unserialised error object.
   */
  public function getErrorDetails($error_id) {
    return $this->connection->select(static::TABLE_ERRORS, 'e')
      ->fields('e', ['data'])
      ->condition('id', $error_id)
      ->range(0, 1)
      ->execute()
      ->fetchField();
  }

  /**
   * Gets current filter values.
   *
   * @return array
   *   Values of filters.
   */
  public function getReportFilters($use_def = FALSE) {
    $defaults = [
      'start' => strtotime(date('Y-m-d', 0)),
      'end' => strtotime(date('Y-m-d', time()) . ' +1day'),
    ];
    if ($use_def) {
      return $defaults;
    }

    $values = !empty($_SESSION['qtools_transport_profiler']['filters']) ?
      $_SESSION['qtools_transport_profiler']['filters'] :
      [];

    foreach ($defaults as $key => $value) {
      if (isset($values[$key])) {
        $defaults[$key] = $values[$key];
      }
    }
    return $defaults;
  }

  /**
   * Builds a base query for Service report.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   Select query.
   */
  public function reportServiceBaseQuery($filters = [], $grouping = NULL) {
    $query = $this->connection->select(static::TABLE_CALLS, 'c')
      ->groupBy('c.sid');

    $query->leftJoin(static::TABLE_SERVICE, 'service', 'c.sid = service.id');
    $query->leftJoin(PerformanceService::TABLE_REQUEST, 'r', 'c.rid = r.id');

    // Apply filters.
    if (!empty($filters['rid'])) {
      $query->condition('r.id', $filters['rid']);
    }
    else {
      $query->condition('r.timestamp', $filters['start'], '>=');
      $query->condition('r.timestamp', $filters['end'], '<=');
    }

    // Add aggregated fields.
    $query->addExpression('MIN(service.id)', 'service_id');
    $query->addExpression('MIN(service.service)', 'service');
    $query->addExpression('SUM(c.call_time) + SUM(c.cache_time) + SUM(c.static_time)', 'total_time');
    $query->addExpression('COUNT(c.rid)', 'request_count');
    $query->addExpression('SUM(c.call_count)', 'call_count');
    $query->addExpression('SUM(c.call_time)/SUM(c.call_count)', 'call_avg');
    $query->addExpression('SUM(c.cache_count)', 'cache_count');
    $query->addExpression('SUM(c.static_count)', 'static_count');
    $query->addExpression('SUM(c.offline_count)', 'offline_count');
    $query->addExpression('SUM(c.error_count)', 'error_count');

    $query = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($this->reportServiceTableHeader($filters, $grouping));

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(static::ITEMS_PER_PAGE);

    return $query;
  }

  /**
   * Return header definition for Service.
   *
   * @return array
   *   Table header.
   */
  public function reportServiceTableHeader($filters = [], $grouping = NULL, $preview = FALSE) {
    $header = [
      'service' => [
        'data' => $this->t('service'),
        'field' => 'service',
      ],
      'total_time' => [
        'data' => $this->t('total_time'),
        'field' => 'total_time',
        'sort' => 'desc',
      ],
      'request_count' => [
        'data' => $this->t('request_count'),
        'field' => 'request_count',
      ],
      'call_count' => [
        'data' => $this->t('call_count'),
        'field' => 'call_count',
      ],
      'call_avg' => [
        'data' => $this->t('call_avg'),
        'field' => 'call_avg',
      ],
      'cache_count' => [
        'data' => $this->t('cache_count'),
        'field' => 'cache_count',
      ],
      'static_count' => [
        'data' => $this->t('static_count'),
        'field' => 'static_count',
      ],
      'offline_count' => [
        'data' => $this->t('offline_count'),
        'field' => 'offline_count',
      ],
      'error_count' => [
        'data' => $this->t('error_count'),
        'field' => 'error_count',
      ],
    ];
    if ($preview) {
      unset($header['request_count']);
    }

    return $header;
  }

  /**
   * Process results of Services report.
   *
   * @return array
   *   Processed items.
   */
  public function reportServiceProcessResults($results, $preview = FALSE) {
    $rows = [];

    // Replace route column with a link to dedicated report.
    foreach ($results as $result) {
      $data = (array) $result;

      // Replace service name with link to other reports if not in preview mode.
      if (!$preview) {
        $link = [
          '#type' => 'link',
          '#title' => $result->service,
          '#url' => Url::fromRoute('qtools_transport_profiler.report.request', [], [
            'query' => [
              'service' => $result->service_id,
            ],
          ]),
        ];
        $data['service'] = $this->renderer->renderRoot($link);
      }
      else {
        unset($data['request_count']);
      }

      unset($data['service_id']);
      $rows[] = [
        'data' => $data,
        'style' => !empty($data['error_count']) ? 'background: salmon;' : '',
      ];

    }
    return $this->processNumericValues($rows);
  }

  /**
   * Builds a base query for Route report.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   Select query.
   */
  public function reportRouteBaseQuery($filters = [], $grouping = NULL) {
    $query = $this->connection->select(PerformanceService::TABLE_REQUEST, 'r')
      ->groupBy('r.rid');

    $query->leftJoin(PerformanceService::TABLE_ROUTE, 'route', 'r.rid = route.id');
    $query->leftJoin(PerformanceService::TABLE_RESPONSE, 'response', 'r.id = response.rid');
    $query->leftJoin(static::TABLE_CALLS, 'calls', 'r.id = calls.rid');

    // Apply filters.
    $query->condition('r.timestamp', $filters['start'], '>=');
    $query->condition('r.timestamp', $filters['end'], '<=');

    // Add aggregated fields.
    $query->addExpression('MIN(route.id)', 'route_id');
    $query->addExpression('MIN(route.route)', 'route');
    $query->addExpression('COUNT(r.rid)', 'request_count');
    $query->addExpression('SUM(response.time)', 'time');
    $query->addExpression('SUM(calls.call_time)', 'call_time');

    $query = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($this->reportRouteTableHeader($filters, $grouping));

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(static::ITEMS_PER_PAGE);

    return $query;
  }

  /**
   * Process results of Route report.
   *
   * @return array
   *   Processed items.
   */
  public function reportRouteProcessResults($results, $filters = [], $grouping = NULL) {
    $rows = [];

    // Replace route column with a link to dedicated report.
    foreach ($results as $result) {
      $data = (array) $result;
      $link = [
        '#type' => 'link',
        '#title' => $result->route,
        '#url' => Url::fromRoute('qtools_transport_profiler.report.request', [], [
          'query' => [
            'route' => $result->route_id,
          ],
        ]),
      ];
      unset($data['route_id']);
      $data['route'] = $this->renderer->renderRoot($link);
      $rows[] = $data;
    }
    return $this->processNumericValues($rows);
  }

  /**
   * Return header definition for Route report.
   *
   * @return array
   *   Table header.
   */
  public function reportRouteTableHeader($filters = [], $grouping = NULL) {
    return [
      'route' => [
        'data' => $this->t('route'),
        'field' => 'route',
      ],
      'request_count' => [
        'data' => $this->t('request_count'),
        'field' => 'request_count',
      ],
      'time' => [
        'data' => $this->t('time'),
        'field' => 'time',
        'sort' => 'desc',
      ],
      'call_time' => [
        'data' => $this->t('call_time'),
        'field' => 'call_time',
      ],
    ];
  }

  /**
   * Builds a base query for Request report.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   Select query.
   */
  public function reportRequestBaseQuery($filters = [], $grouping = NULL) {
    // Base table.
    $query = $this->connection->select(PerformanceService::TABLE_REQUEST, 'r');

    // Join additional resources.
    $query->leftJoin(PerformanceService::TABLE_ROUTE, 'route', 'r.rid = route.id');
    $query->leftJoin(PerformanceService::TABLE_RESPONSE, 'response', 'r.id = response.rid');
    $query->leftJoin(static::TABLE_CALLS, 'calls', 'r.id = calls.rid');
    $query->leftJoin(static::TABLE_ERRORS, 'errors', 'r.id = errors.rid');

    // Apply filters.
    $query->condition('r.timestamp', $filters['start'], '>=');
    $query->condition('r.timestamp', $filters['end'], '<=');

    if (!empty($filters['route'])) {
      $query->condition('r.rid', $filters['route'], '=');
    }
    if (!empty($filters['service'])) {
      $query->condition('calls.sid', $filters['service'], '=');
    }

    // Add grouping.
    $query = $query->groupBy('r.id');

    // Add fields.
    $query->addExpression('MIN(r.id)', 'id');
    $query->addExpression('MIN(r.timestamp)', 'timestamp');
    $query->addExpression('MIN(response.code)', 'code');
    $query->addExpression('MIN(response.report)', 'report');
    $query->addExpression('MIN(r.uri)', 'uri');
    $query->addExpression('MIN(response.time)', 'time');
    $query->addExpression('MIN(response.memory)', 'memory');
    $query->addExpression('SUM(calls.call_time)', 'call_time');
    $query->addExpression('SUM(calls.error_count)', 'error_count');
    $query->addExpression('COUNT(errors.id)', 'error_log_count');

    $query = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($this->reportRequestTableHeader($filters, $grouping));

    $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(static::ITEMS_PER_PAGE);

    return $query;
  }

  /**
   * Process results of Request report.
   *
   * @return array
   *   Processed items.
   */
  public function reportRequestProcessResults($results, $filters = [], $grouping = NULL) {
    $rows = [];

    // Replace route column with a link to dedicated report.
    foreach ($results as $result) {
      $data = (array) $result;
      $link = [
        '#type' => 'link',
        '#title' => $result->uri,
        '#url' => Url::fromUri('internal:' . $result->uri),
        '#attributes' => [
          'target' => '_blank',
        ],
      ];
      $data['uri'] = $this->renderer->renderRoot($link);

      $data['timestamp'] = date('Y-m-d H:i:s Z', $data['timestamp']);
      if ($result->report > 0) {
        $link = [
          '#type' => 'link',
          '#title' => $data['timestamp'],
          '#url' => Url::fromRoute('qtools_profiler.admin.report_profile', ['id' => $data['id']]),
          '#attributes' => [
            'target' => '_blank',
          ],
        ];
        $data['timestamp'] = $this->renderer->renderRoot($link);
      }

      if ($data['call_time'] != 0) {
        $link = [
          '#type' => 'link',
          '#title' => round($data['call_time'], 2),
          '#url' => Url::fromRoute('qtools_transport_profiler.report.service', [], [
            'query' => [
              'request' => $result->id,
            ],
          ]),
        ];
        $data['call_time'] = $this->renderer->renderRoot($link);
      }

      if (!empty($data['error_log_count'])) {
        $link = [
          '#type' => 'link',
          '#title' => $this->t('@count (see details)', ['@count' => $result->error_count])->render(),
          '#url' => Url::fromRoute('qtools_transport_profiler.report.error', [], [
            'query' => [
              'route' => !empty($filters['route']) ? $filters['route'] : NULL,
              'service' => !empty($filters['service']) ? $filters['service'] : NULL,
              'request' => $result->id,
            ],
          ]),
        ];
        $data['error_count'] = $this->renderer->renderRoot($link);
      }
      unset($data['id']);
      unset($data['error_log_count']);
      unset($data['report']);
      $rows[] = [
        'data' => $data,
        'style' => !empty($data['error_count']) ? 'background: salmon;' : '',
      ];

    }
    return $this->processNumericValues($rows);
  }

  /**
   * Return header definition for requests.
   *
   * @return array
   *   Table header.
   */
  public function reportRequestTableHeader($filters = [], $grouping = NULL) {
    return [
      'timestamp' => [
        'data' => $this->t('timestamp'),
        'field' => 'timestamp',
        'sort' => 'desc',
      ],
      'id' => [
        'data' => $this->t('code'),
        'field' => 'code',
      ],
      'uri' => [
        'data' => $this->t('uri'),
        'field' => 'uri',
      ],
      'time' => [
        'data' => $this->t('time'),
        'field' => 'time',
      ],
      'memory' => [
        'data' => $this->t('memory'),
        'field' => 'memory',
      ],
      'call_time' => [
        'data' => $this->t('call_time'),
        'field' => 'call_time',
      ],
      'error_count' => [
        'data' => $this->t('error_count'),
        'field' => 'error_count',
      ],
    ];
  }

  /**
   * Builds a base query for Error report.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   Select query.
   */
  public function reportErrorBaseQuery($filters = [], $grouping = NULL, $pager = TRUE) {
    // Base table.
    $query = $this->connection->select(static::TABLE_ERRORS, 'e');

    // Join additional resources.
    $query->leftJoin(PerformanceService::TABLE_REQUEST, 'r', 'e.rid = r.id');
    $query->leftJoin(static::TABLE_CALLS, 'c', 'c.rid = r.id');
    $query->leftJoin(static::TABLE_SERVICE, 'service', 'service.id = e.sid');

    // Apply filters.
    $query->condition('r.timestamp', $filters['start'], '>=');
    $query->condition('r.timestamp', $filters['end'], '<=');

    if (!empty($filters['route'])) {
      $query->condition('r.rid', $filters['route'], '=');
    }
    if (!empty($filters['service'])) {
      $query->condition('c.sid', $filters['service'], '=');
    }
    if (!empty($filters['request'])) {
      $query->condition('e.rid', $filters['request'], '=');
    }

    $query->groupBy('e.id');

    // Add fields.
    $query->addExpression('MIN(r.timestamp)', 'timestamp');
    $query->addExpression('MIN(e.code)', 'code');
    $query->addExpression('MIN(e.cache_op)', 'cache_op');
    $query->addExpression('MIN(e.time)', 'time');
    $query->addExpression('MIN(e.message)', 'message');
    $query->addExpression('MIN(service.service)', 'service');
    $query->addExpression('IF(MIN(e.data) IS NULL, 0, e.id)', 'details');

    $query = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($this->reportErrorTableHeader($filters, $grouping));

    if ($pager) {
      $query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit(static::ITEMS_PER_PAGE);
    }

    return $query;
  }

  /**
   * Process results of Error report.
   *
   * @return array
   *   Processed items.
   */
  public function reportErrorProcessResults($results, $filters = [], $preview = FALSE) {
    $rows = [];
    $request = !empty($filters['request']) ? $filters['request'] : NULL;

    // Replace route column with a link to dedicated report.
    foreach ($results as $result) {
      $data = (array) $result;
      $data['timestamp'] = date('Y-m-d H:i:s Z', $data['timestamp']);
      if (!empty($data['details'])) {
        if ($preview) {
          $link = [
            '#type' => 'link',
            '#title' => $this->t('view #@details', ['@details' => $result->details])->render(),
            '#url' => Url::fromRoute('qtools_profiler.preview.details', [], [
              'query' => [
                'request' => $request,
                'error' => $result->details,
              ],
            ]),
          ];
        }
        else {
          $link = [
            '#type' => 'link',
            '#title' => $this->t('view #@details', ['@details' => $result->details])->render(),
            '#url' => Url::fromRoute('qtools_transport_profiler.report.error', [], [
              'query' => [
                'route' => !empty($filters['route']) ? $filters['route'] : NULL,
                'service' => !empty($filters['service']) ? $filters['service'] : NULL,
                'request' => $request,
                'error' => $result->details,
              ],
            ]),
          ];
        }
        $data['details'] = $this->renderer->renderRoot($link);
      }
      else {
        $data['details'] = $this->t('not available')->render();
      }

      // Merge cached and time columns.
      if ($data['cache_op'] == QtoolsCacheHelper::CACHE_FOUND_PERSISTENT) {
        $data['time'] = $this->t('Cached')->render();
      }
      elseif ($data['cache_op'] == QtoolsCacheHelper::CACHE_FOUND_STATIC) {
        $data['time'] = '-';
      }
      unset($data['cache_op']);
      $rows[] = [
        'data' => $data,
        'style' => !empty($data['code']) ? 'background: salmon;' : '',
      ];
    }

    return $this->processNumericValues($rows);
  }

  /**
   * Return header definition for Error report.
   *
   * @return array
   *   Table header.
   */
  public function reportErrorTableHeader($filters = [], $grouping = NULL, $preview = FALSE) {
    return [
      'timestamp' => [
        'data' => $this->t('timestamp'),
        'field' => 'timestamp',
      ],
      'code' => [
        'data' => $this->t('error'),
        'field' => 'code',
      ],
      'time' => [
        'data' => $this->t('time'),
        'field' => 'time',
      ],
      'message' => [
        'data' => $this->t('message'),
        'field' => 'message',
      ],
      'service' => [
        'data' => $this->t('service'),
        'field' => 'service',
      ],
      'details' => [
        'data' => $this->t('details'),
        'field' => 'details',
        'sort' => 'desc',
      ],
    ];
  }

  /**
   * Apply precision to numeric values.
   */
  protected function processNumericValues($rows) {
    foreach ($rows as $row => $data) {
      if (!empty($data['data'])) {
        foreach ($data['data'] as $key => $value) {
          if (is_numeric($value)) {
            $data['data'][$key] = round($value, 2);
          }
        }
      }
      else {
        foreach ($data as $key => $value) {
          if (is_numeric($value)) {
            $data[$key] = round($value, 2);
          }
        }
      }
      $rows[$row] = $data;
    }

    return $rows;
  }

  /**
   * Apply precision to numeric values.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   Query with filters applied.
   */
  protected function applyFilters($query, $filters = []) {
    foreach ($filters as $field => $value) {
      $query->condition($field, $value);
    }
    return $query;
  }

}
