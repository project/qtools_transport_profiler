<?php

namespace Drupal\qtools_transport_profiler\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\qtools_transport_profiler\ReportService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for requests report.
 */
class RequestReportController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected ReportService $reportService,
    protected RequestStack $requestStack,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(ReportService::class),
      $container->get('request_stack')
    );
  }

  /**
   * Returns page content.
   *
   * @return array
   *   Page array.
   */
  public function pageBuild() {

    // Get filter values.
    $route = $this->requestStack->getCurrentRequest()->query->get('route');
    $service = $this->requestStack->getCurrentRequest()->query->get('service');

    // Get filter form.
    $build['form'] = $this->formBuilder()->getForm('\Drupal\qtools_transport_profiler\Form\ReportFilterForm');

    // Get report table.
    $filters = [
      'service' => $service,
      'route' => $route,
    ] + $this->reportService->getReportFilters();
    $header = $this->reportService->reportRequestTableHeader($filters);
    $query = $this->reportService->reportRequestBaseQuery($filters);
    $results = $query->execute()->fetchAll();
    $rows = $this->reportService->reportRequestProcessResults($results, $filters);

    $build['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#cache' => [
        'max-age' => -1,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

}
