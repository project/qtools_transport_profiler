<?php

namespace Drupal\qtools_transport_profiler\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\qtools_transport_profiler\ReportService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for services reports.
 */
class ServiceReportController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected ReportService $reportService,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(ReportService::class),
    );
  }

  /**
   * Returns page content.
   *
   * @return array
   *   Page array.
   */
  public function pageBuild(Request $request) {

    $request_id = $request->query->get('request');

    // Get filter form.
    $build['form'] = $this->formBuilder()->getForm('\Drupal\qtools_transport_profiler\Form\ReportFilterForm');

    // Get report table.
    $filters = $this->reportService->getReportFilters();
    if (!empty($request_id)) {
      $filters['rid'] = $request_id;
    }

    $header = $this->reportService->reportServiceTableHeader($filters);
    $query = $this->reportService->reportServiceBaseQuery($filters);
    $results = $query->execute()->fetchAll();
    $rows = $this->reportService->reportServiceProcessResults($results);

    $build['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#cache' => [
        'max-age' => -1,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

  /**
   * Preview report.
   */
  public function previewReport(Request $request) {
    $request_id = $request->query->get('request');

    // Get report table.
    $filters = $this->reportService->getReportFilters(TRUE);
    if (!empty($request_id)) {
      $filters['rid'] = $request_id;
    }

    $header = $this->reportService->reportServiceTableHeader($filters, NULL, TRUE);
    $query = $this->reportService->reportServiceBaseQuery($filters);
    $results = $query->execute()->fetchAll();
    $rows = $this->reportService->reportServiceProcessResults($results, TRUE);

    $build['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#cache' => [
        'max-age' => -1,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

}
