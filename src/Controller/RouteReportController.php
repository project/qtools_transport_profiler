<?php

namespace Drupal\qtools_transport_profiler\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\qtools_transport_profiler\ReportService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for routes report.
 */
class RouteReportController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected ReportService $reportService,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(ReportService::class),
      $container->get('form_builder')
    );
  }

  /**
   * Returns page content.
   *
   * @return array
   *   Page array.
   */
  public function pageBuild() {

    // Get filter form.
    $build['form'] = $this->formBuilder()->getForm('\Drupal\qtools_transport_profiler\Form\ReportFilterForm');

    // Get report table.
    $filters = $this->reportService->getReportFilters();
    $header = $this->reportService->reportRouteTableHeader($filters);
    $query = $this->reportService->reportRouteBaseQuery($filters);
    $results = $query->execute()->fetchAll();
    $rows = $this->reportService->reportRouteProcessResults($results);

    $build['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#cache' => [
        'max-age' => -1,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

  /**
   * Returns page content.
   *
   * @return array
   *   Page array.
   */
  public function pageBuildDetails($route) {

    // Get filter form.
    $build['form'] = $this->formBuilder()->getForm('\Drupal\qtools_transport_profiler\Form\ReportFilterForm');

    // Get report table.
    $filters = $this->reportService->getReportFilters();
    $filters['route'] = $route;

    $header = $this->reportService->reportRouteTableHeader($filters);
    $query = $this->reportService->reportRouteBaseQuery($filters);
    $results = $query->execute()->fetchAll();
    $rows = $this->reportService->reportRouteProcessResults($results);

    $build['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#cache' => [
        'max-age' => -1,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

}
