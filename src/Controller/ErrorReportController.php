<?php

namespace Drupal\qtools_transport_profiler\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\Element\StatusMessages;
use Drupal\qtools_common\QtoolsArrayHelper;
use Drupal\qtools_transport_profiler\ReportService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for error report.
 */
class ErrorReportController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected ReportService $reportService,
    protected RequestStack $requestStack,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(ReportService::class),
      $container->get('request_stack')
    );
  }

  /**
   * Returns page content.
   *
   * @return array
   *   Page array.
   */
  public function pageBuild(Request $request) {

    // Get filter values.
    $route = $request->query->get('route');
    $service = $request->query->get('service');
    $error = $request->query->get('error');
    $inspect_key = $request->query->get('inspect');

    // Load error if not empty.
    if (!empty($error)) {
      $error_details = $this->reportService->getErrorDetails($error);
      if (!empty($error_details)) {
        $error_details = Json::decode($error_details);
        $this->reportService->export($this->messenger(), $error_details);
        if (!empty($inspect_key)) {
          $inspect = QtoolsArrayHelper::getValue($error_details, explode('][', $inspect_key));
          $this->reportService->export($this->messenger(), $inspect, $inspect_key);
        }
      }
    }

    // Get filter form.
    $build['form'] = $this->formBuilder()->getForm('\Drupal\qtools_transport_profiler\Form\ReportFilterForm');

    // Get report table.
    $filters = [
      'service' => $service,
      'route' => $route,
    ] + $this->reportService->getReportFilters();
    $header = $this->reportService->reportErrorTableHeader($filters);
    $query = $this->reportService->reportErrorBaseQuery($filters);
    $results = $query->execute()->fetchAll();
    $rows = $this->reportService->reportErrorProcessResults($results, $filters);

    $build['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#cache' => [
        'max-age' => -1,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

  /**
   * Preview report.
   */
  public function previewReport(Request $request) {
    $request_id = $request->query->get('request');
    $error_id = $request->query->get('error');
    $inspect_key = $request->query->get('inspect');

    // Load error if not empty.
    if (!empty($error_id)) {
      $error_details = $this->reportService->getErrorDetails($error_id);
      if (!empty($error_details)) {
        $error_details = Json::decode($error_details);
        $this->reportService->export($this->messenger(), $error_details);
        if (!empty($inspect_key)) {
          $inspect = QtoolsArrayHelper::getValue($error_details, explode('][', $inspect_key));
          $this->reportService->export($this->messenger(), $inspect, $inspect_key);
        }
      }
    }

    // Get report table.
    $filters = $this->reportService->getReportFilters(TRUE);
    if (!empty($request_id)) {
      $filters['request'] = $request_id;
    }

    $header = $this->reportService->reportErrorTableHeader($filters, NULL, TRUE);
    $query = $this->reportService->reportErrorBaseQuery($filters, NULL, FALSE);
    $results = $query->execute()->fetchAll();
    $rows = $this->reportService->reportErrorProcessResults($results, $filters, TRUE);

    $build['messages'] = StatusMessages::renderMessages();

    $build['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#cache' => [
        'max-age' => -1,
      ],
      '#attached' => [
        'library' => [
          'qtools_transport_profiler/var-dumper-extender',
        ],
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

}
