<?php

namespace Drupal\qtools_transport_profiler;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\qtools_transport\QtoolsTransportService;
use Drupal\qtools_transport\Utility\QtoolsTransportRequest;
use Drupal\qtools_transport\Utility\QtoolsTransportResponse;
use Drupal\qtools_common\QtoolsCacheHelper;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\qtools_profiler\PerformanceService as ProfilerPerformanceService;

/**
 * Wraps QtoolsTransportService to track request stats.
 */
class TransportServiceWrapper extends QtoolsTransportService {

  /**
   * Drupal\qtools_transport_profiler\PerformanceService.
   *
   * @var \Drupal\qtools_transport_profiler\PerformanceService
   */
  protected $performanceService;

  /**
   * Drupal\qtools_profiler\PerformanceService.
   *
   * @var \Drupal\qtools_profiler\PerformanceService
   */
  protected $profilerPerformanceService;

  /**
   * Drupal\Component\Datetime\TimeInterface.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Holds info about called services.
   *
   * @var array
   */
  protected $performanceStats;

  /**
   * Temp storage of service response data.
   *
   * @var mixed
   */
  protected $lastParsedContent;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ClientFactory $httpClientFactory,
    RequestStack $requestStack,
    UuidInterface $uuid
  ) {
    parent::__construct($httpClientFactory, $requestStack, $uuid);

    $this->performanceService = \Drupal::service(PerformanceService::class);
    $this->profilerPerformanceService = \Drupal::service(ProfilerPerformanceService::class);
    $this->performanceStats = [];
    $this->time = \Drupal::time();
  }

  /**
   * {@inheritdoc}
   */
  protected function parseResponseContent(ResponseInterface $response, QtoolsTransportRequest $transportRequest): mixed {
    $parsed_content = parent::parseResponseContent($response, $transportRequest);

    // Temp store data.
    $this->lastParsedContent = $parsed_content;

    return $parsed_content;
  }

  /**
   * {@inheritdoc}
   */
  public function executeTransportRequest(QtoolsTransportRequest $transportRequest): QtoolsTransportResponse {
    $order = &drupal_static(static::class . '.call_order', 0);
    $order++;

    $this->lastParsedContent = NULL;

    // Unlink variable to avoid sub-requests mess.
    $real_order = $order;

    // Call service and track time spent.
    $start = $this->time->getCurrentMicroTime();
    $transportResponse = parent::executeTransportRequest($transportRequest);
    $elapsed = $this->time->getCurrentMicroTime() - $start;

    // Extract valuable info.
    $cached_op = $transportResponse->getMeta('cached_op');

    // Store info in the cache.
    $service_key = $this->serviceKey($transportRequest);
    $logging_mode = $this->performanceService->getLoggingMode();
    if ($this->performanceService->enabled()) {
      if ($cached_op !== QtoolsCacheHelper::CACHE_FOUND_STATIC) {
        $stats = [
          'order' => $real_order,
          'mode' => $cached_op,
          'time' => $elapsed,
          'details' => $this->cleanResponse($transportResponse, $this->lastParsedContent),
          'static_time' => 0,
          'static_count' => 0,
          'error' => $this->cleanError($transportResponse),
        ];
        $this->performanceStats[$service_key][] = $stats;
      }
      elseif (!empty($this->performanceStats[$service_key])) {
        // If static calls should be tracked, store simplified object.
        if ($logging_mode == PerformanceService::LOG_DETAILS_ALL_WITH_STATIC) {
          $stats = [
            'order' => $order,
            'mode' => $cached_op,
            'time' => 0,
            'details' => $this->cleanResponse($transportResponse, ''),
            'static_time' => 0,
            'static_count' => 0,
            'error' => $this->cleanError($transportResponse),
          ];
          $this->performanceStats[$service_key][] = $stats;
        }
        else {
          // If static calls are not stored, we update totals in the
          // original request. Note that static calls are ALWAYS runs after
          // at least one non-static one (by definition fo static call).
          $count = count($this->performanceStats[$service_key]);
          $last = &$this->performanceStats[$service_key][$count - 1];
          $last['static_time'] += $elapsed;
          $last['static_count'] += 1;
        }
      }
      else {
        // Something went wrong, static calls can't appear before non-static.
      }
    }

    // Clean temp data.
    $this->lastParsedContent = NULL;

    return $transportResponse;
  }

  /**
   * Returns stats about called services.
   */
  public function getPerformanceStats(): array {
    return $this->performanceStats;
  }

  /**
   * Cleans response from extra stuff.
   */
  protected function cleanResponse(QtoolsTransportResponse $transportResponse, mixed $lastParsedContent): ?array {
    $logging_mode = $this->performanceService->getLoggingMode();
    $trace_length = $this->performanceService->confGet('trace_length');

    // Exit if no need to log at all.
    if ($logging_mode == PerformanceService::LOG_DETAILS_NONE) {
      return NULL;
    }

    // Exit if we must log only errors, but we have none.
    if ($logging_mode == PerformanceService::LOG_DETAILS_ERRORS && !$transportResponse->hasErrors()) {
      return NULL;
    }

    // Log details
    // PerformanceService::LOG_DETAILS_ALL,
    // PerformanceService::LOG_DETAILS_ALL_WITH_STATIC.
    $clean = [
      'request' => $transportResponse->getTransportRequest()->toRequestParams(),
      'curl' => $transportResponse->getTransportRequest()->toCurlCommand(),
      'status_code' => $transportResponse->getResponseStatusCode(),
      'phrase' => $transportResponse->getResponsePhrase(),
      'headers' => $transportResponse->getResponseHeaders(),
      'content' => $lastParsedContent,
      'parsed' => $transportResponse->getParsed(),
      'exceptions' => $transportResponse->getExceptions(),
      'errors' => $transportResponse->getErrors(),
      'meta' => $transportResponse->getMetaAll(),
    ];

    // Add backtrace if needed.
    if (!empty($trace_length)) {
      $bt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
      if ($trace_length > 0) {
        $bt = array_slice($bt, 3, $trace_length);
      }
      else {
        $bt = array_slice($bt, 3);
      }

      $log = [];
      foreach ($bt as $step) {
        $log[] = $step['function'] . ' ( ' . (!empty($step['line']) ? $step['line'] : '?') . ' ) ' . (!empty($step['class']) ? $step['class'] : '');
      }
      $clean['trace'] = $log;
    }

    return $clean;
  }

  /**
   * Cleans response from extra stuff.
   */
  protected function cleanError(QtoolsTransportResponse $transportResponse): ?array {
    // Exit if no errors.
    if (!$transportResponse->hasErrors()) {
      return NULL;
    }

    // Error summary.
    return [
      'code' => $transportResponse->getResponseStatusCode(),
      'message' => implode(', ', $transportResponse->getErrorsTexts()),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function getConfigTraitBundle(): string {
    // We need to specify parent class here so that configuration overrides
    // are properly applied.
    return QtoolsTransportService::class;
  }

}
