/**
 * @file
 * Behaviours for profiler extender.
 */

// Namespace.
var qtoolsProfilerVarDumperExtender = {};

(function (Drupal, $, once) {
  var extender = qtoolsProfilerVarDumperExtender;

  /**
   * Handle pageLoad.
   */
  extender.handlerMissingDumps = function (context, settings) {
    // Add drill down links.
    $('.sf-dump-note', context).filter(function () { return jQuery(this).next('a').length === 0 }).each(function () {
      // Add inspect icon to all ancestors.
      var $inspect_link = '<span class="var-dump-drill-down" style="color: white; background: green; padding-left: 5px; padding-right: 5px; margin-right: 5px; cursor:pointer;">&#9660;</span>';
      $(this).before($inspect_link);

      var $parents = $(this).parents('.sf-dump-expanded, .sf-dump-compact')
        .not('.var-dump-processed');

      for (var i = 0; i < $parents.length - 1; i++) {
        var $parent = $parents.eq(i);
        $parent.addClass('var-dump-processed');
        $parent.prev().prev().before($inspect_link);;
      }
    });

    // Handle drill down.
    $('.var-dump-drill-down', context).click(function () {
      var keys = [];
      var $this = $(this);
      keys.push($this.prev('.sf-dump-key, .sf-dump-index').text());

      // Find key path to the clicked element.
      var $parents = $(this).parents('.sf-dump-expanded');
      for (var i = 0; i < $parents.length - 1; i++) {
        var $parent = $parents.eq(i);
        keys.push($parent.prev().prev().prev().prev('.sf-dump-key, .sf-dump-index').text());
      }
      var inspect = keys.reverse().join('][');

      // Get current inspection path.
      var branch = $(this).parents('.messages__item').eq(0).find('.var-dump-drill-up').data('branch');
      if (branch) {
        inspect = branch + '][' + inspect;
      }

      // WE don't care about IE in relation to services inspection.
      var url = new URL(location.href);
      url.searchParams.set('inspect', inspect);
      location.href = url;
    })

    // Add drill up links.
    const $varDumpExtender =  $(once('var-dump-extender', '.messages  .messages__item'));
    $varDumpExtender.each(function () {
      var caption = this.firstChild.nodeValue || '';
      if (caption.indexOf(' =>') !== -1) {
        var branch = caption.split('=>')[0].trim();
        var $inspect_link = '<span class="var-dump-drill-up" data-branch="' + branch + '" style="color: white; background: green; padding-left: 5px; padding-right: 5px; margin-right: 5px; cursor:pointer;">&#9650;</span>';
        $(this).prepend($inspect_link);
      }
    });

    // Handle drill up links.
    $('.var-dump-drill-up', context).click(function () {
      var branch = $(this).data('branch');
      var chunks = branch.split('][');
      chunks.pop();
      branch = chunks.join('][');

      // WE don't care about IE in relation to services inspection.
      var url = new URL(location.href);
      url.searchParams.set('inspect', branch);
      location.href = url;
    });
  };

  /**
   * Handle pageLoad.
   */
  extender.handlerMissingDumps1 = function (context, settings) {
    $('.sf-dump-note', context).filter(function () { return $(this).parent('.sf-dump').length === 0 }).each(function () {
      var $inspect_link = $('<span class="var-dump-drill-down" style="color: white; background: green; padding-left: 5px; padding-right: 5px; margin-right: 5px; cursor:pointer;">i</span>');
      $(this).before($inspect_link);
    });

  };

  /**
   * Register behavior.
   */
  Drupal.behaviors.qtoolsProfilerVarDumperExtender = {
    attach: function (context, settings) {
      extender.handlerMissingDumps(context, settings);
    }
  };

})(Drupal, jQuery, once);
