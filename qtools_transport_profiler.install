<?php

/**
 * @file
 * Install hooks.
 */

use Drupal\Core\Database\Database;

/**
 * Implements hook_schema().
 */
function qtools_transport_profiler_schema() {
  $schema['qtools_transport_profiler_errors'] = [
    'description' => 'Table for services errors.',
    'fields' => [
      'id' => [
        'description' => 'The primary identifier for error.',
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'rid' => [
        'description' => 'The primary identifier for request report.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'sid' => [
        'description' => 'Service id.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'code' => [
        'description' => 'Error code.',
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ],
      'cache_op' => [
        'description' => 'Caching operation.',
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'time' => [
        'description' => 'Call time.',
        'type' => 'float',
        'not null' => TRUE,
        'default' => 0,
      ],
      'message' => [
        'description' => 'Error message.',
        'type' => 'varchar',
        'length' => 256,
        'not null' => TRUE,
      ],
      'data' => [
        'description' => 'Error data.',
        'type' => 'text',
        'size' => 'big',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'rid' => ['rid'],
      'sid' => ['sid'],
    ],
  ];
  $schema['qtools_transport_profiler_service'] = [
    'description' => 'Table for services names.',
    'fields' => [
      'id' => [
        'description' => 'The primary identifier for call.',
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'service' => [
        'description' => 'Service name.',
        'type' => 'varchar',
        'length' => 256,
        'not null' => TRUE,
      ],
    ],
    'primary key' => ['id'],
  ];
  $schema['qtools_transport_profiler_calls'] = [
    'description' => 'Table for storing services call.',
    'fields' => [
      'id' => [
        'description' => 'The primary identifier for call.',
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'rid' => [
        'description' => 'The primary identifier for request.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'sid' => [
        'description' => 'Service id.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'call_count' => [
        'description' => 'Amount of service requests.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'call_time' => [
        'description' => 'Time spent on loading data from services.',
        'type' => 'float',
        'size' => 'big',
        'not null' => TRUE,
      ],
      'cache_count' => [
        'description' => 'Amount of cached requests.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'cache_time' => [
        'description' => 'Time spent on loading cached data.',
        'type' => 'float',
        'size' => 'big',
        'not null' => TRUE,
      ],
      'static_count' => [
        'description' => 'Amount of statically cached requests.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'static_time' => [
        'description' => 'Time spent on loading statically cached data.',
        'type' => 'float',
        'size' => 'big',
        'not null' => TRUE,
      ],
      'offline_count' => [
        'description' => 'Amount of offline requests.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'error_count' => [
        'description' => 'Errors count.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'indexes' => [
      'rid' => ['rid'],
      'sid' => ['sid'],
    ],
    'primary key' => ['id'],
  ];
  return $schema;
}

/**
 * Implements hook_update_N().
 */
function qtools_transport_profiler_update_8001(&$sandbox) {
  $qtools_transport_profiler_errors = [
    'description' => 'Table for services errors.',
    'fields' => [
      'id' => [
        'description' => 'The primary identifier for error.',
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'rid' => [
        'description' => 'The primary identifier for request report.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'sid' => [
        'description' => 'Service id.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'code' => [
        'description' => 'Error code.',
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
      ],
      'message' => [
        'description' => 'Error message.',
        'type' => 'varchar',
        'length' => 256,
        'not null' => TRUE,
      ],
      'data' => [
        'description' => 'Error data.',
        'type' => 'text',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'rid' => ['rid'],
      'sid' => ['sid'],
    ],
  ];

  $schema = Database::getConnection()->schema();
  $schema->createTable('qtools_transport_profiler_errors', $qtools_transport_profiler_errors);
}

/**
 * Implements hook_update_N().
 *
 * Extends data field size.
 */
function qtools_transport_profiler_update_8002(&$sandbox) {
  $spec = [
    'description' => 'Request data.',
    'type' => 'text',
    'size' => 'big',
  ];
  $schema = Database::getConnection()->schema();
  $schema->dropField('qtools_transport_profiler_errors', 'data');
  $schema->addField('qtools_transport_profiler_errors', 'data', $spec);
}

/**
 * Implements hook_update_N().
 *
 * Adds cache_op field.
 */
function qtools_transport_profiler_update_8003(&$sandbox) {
  $spec = [
    'description' => 'Caching operation.',
    'type' => 'int',
    'unsigned' => FALSE,
    'not null' => TRUE,
    'default' => 0,
  ];
  $schema = Database::getConnection()->schema();
  $schema->addField('qtools_transport_profiler_errors', 'cache_op', $spec);
}

/**
 * Implements hook_update_N().
 *
 * Adds time spend field.
 */
function qtools_transport_profiler_update_8004(&$sandbox) {
  $spec = [
    'description' => 'Call time.',
    'type' => 'float',
    'not null' => TRUE,
    'default' => 0,
  ];
  $schema = Database::getConnection()->schema();
  $schema->addField('qtools_transport_profiler_errors', 'time', $spec);
}
